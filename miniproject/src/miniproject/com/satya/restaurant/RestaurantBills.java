package miniproject.com.satya.restaurant;

import java.util.*;

public class RestaurantBills {
	
	private String name;
	private List<Item> items;
	private double cost;
	private Date time;
	
	public RestaurantBills() {}
	
	public RestaurantBills(String name, List<Item> items, double cost, Date time) throws IllegalArgumentException {
		super();
		
		this.name = name;
		this.items = items;
		if(cost<0)
		{
			throw new IllegalArgumentException("exception occured, cost cannot less than zero");
		}
		this.cost = cost;
		this.time = time;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> selectedItems) {
		this.items = selectedItems;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date date) {
		this.time = date;
	}
	

}
