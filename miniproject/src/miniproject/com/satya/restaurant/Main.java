package miniproject.com.satya.restaurant;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import miniproject.com.satya.restaurant.*;




public class Main {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Date time=new Date();
		
		List<RestaurantBills> bills = new ArrayList<RestaurantBills>();
		boolean finalOrder;

		List<Item> items = new ArrayList<Item>();

		Item item1 = new Item(1, "Rajma Chawal", 2, 150.0);
		Item item2 = new Item(2, "Momos", 2, 190.0);
		Item item3 = new Item(3, "Red Thai Curry", 2, 180.0);
		Item item4 = new Item(4, "Chaap", 2, 190.0);
		Item item5 = new Item(5, "Chilli Potato", 1, 250.0);

		items.add(item1);
		items.add(item2);
		items.add(item3);
		items.add(item4);
		items.add(item5); 

		System.out.println(
				"$$Welcome to Surabi Restaurant\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

		while (true) {
			System.out.println("------Please Enter the Credentials-------");

			System.out.println("Email = ");
			String email = sc.next();

			System.out.println("Password = ");
			String password = sc.next();

			String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);

			System.out.println("Please Enter A if you are Admin and U if you are User ,L to logout");
			String aorU = sc.next();

			RestaurantBills bill = new RestaurantBills();
			List<Item> selectedItems = new ArrayList<Item>();
			
			double totalCost = 0;
			
			Date date=new Date();
			
			ZonedDateTime time1 = ZonedDateTime.now();
			DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss zzz yyyy");
			String currentTime = time1.format(f);

			if (aorU.equals("U") || aorU.equals("u")) {

				System.out.println("Welcome Mr. " + email);
				do {
					System.out.println("Today's Menu :- ");
					items.stream().forEach(i -> System.out.println(i));
					System.out.println("Enter the Menu Item Code");
					int code = sc.nextInt();
					
					if (code == 1) {
						selectedItems.add(item1);
						totalCost += item1.getItemPrice();
					}

					else if (code == 2) {
						selectedItems.add(item2);
						totalCost += item2.getItemPrice();
					} else if (code == 3) {
						selectedItems.add(item3);
						totalCost += item3.getItemPrice();
					} else if (code == 4) {
						selectedItems.add(item4);
						totalCost += item4.getItemPrice();
					} else {
						selectedItems.add(item5);
						totalCost += item5.getItemPrice();
					}
					
					System.out.println("Press 0 to show bill\nPress 1 to order more");
					int opt = sc.nextInt();
					if (opt == 0)
						finalOrder = false;
					else
						finalOrder = true;

				} while (finalOrder);
				

				System.out.println("Thanks Mr " + name + " for dining in with Surabi ");
				System.out.println("Items you have Selected");
				selectedItems.stream().forEach(e -> System.out.println(e));
				System.out.println("Your Total bill will be " + totalCost);

				bill.setName(name);
				bill.setCost(totalCost);
				bill.setItems(selectedItems);
				bill.setTime(date);
				bills.add(bill);

			} else if (aorU.equals("A") || aorU.equals("a")) {
				System.out.println("Welcome Admin");
				System.out.println(
						"Press 1 to see all the bills for today\nPress 2 to see all the bills for this month\nPress 3 to see all the bills");
				int value = sc.nextInt();
				switch (value) {
				case 1:
					if ( !bills.isEmpty()) {
						for (RestaurantBills b : bills) {
							if(b.getTime().getDate()==time.getDate()) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills today.!");
					break;

				case 2:
					if (!bills.isEmpty()) {
						for (RestaurantBills b : bills) {
							if (b.getTime().getMonth()==time.getMonth()) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills for this month.!");
					break;

				case 3:
					if (!bills.isEmpty()) {
						for (RestaurantBills b : bills) {
							System.out.println("\nUsername :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date " + b.getTime() + "\n");
						}
					} else
						System.out.println("No Bills.!");

					break;

				default:
					System.out.println("Invalid Option");
					System.exit(1);
				}
			} else if (aorU.equals("L") || aorU.equals("l")) {
				System.exit(1);
				
			}else  {
				System.out.println("Invalid Entry");
			}

		}

	}
}
