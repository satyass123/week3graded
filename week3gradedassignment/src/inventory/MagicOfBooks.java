package inventory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MagicOfBooks {
	private Map<Integer, Book> books;

	MagicOfBooks() {
		this.books = new HashMap<>();
	}

	public boolean addBook(int id, Book book) {
		books.put(id, book);
		return true;
	}

	public boolean deleteBook(int id) {
		 return books.remove(id) !=null;
	}

	public void updateBook(int id, Book book) {
		books.put(id, book);
		
	}

	public void display() {
		System.out.println(books);
	}

	public void countOfBooks() {
		System.out.println(books.size());
	}

	public void displayByGenre(String genre) {
		Collection<Book> books2 = books.values();
		books2.stream().filter(book -> book.getGenre().equalsIgnoreCase(genre)).forEach(System.out::println);
	}

	public void displayInOrder() {
		List<Book> books2 = new ArrayList<>();
		for (Map.Entry<Integer, Book> map : books.entrySet()) {
			books2.add(map.getValue());
		}

		Collections.sort(books2, (book1, book2) -> book1.getPrice() > book2.getPrice() ? 1 : -1);
		System.out.println(books2);
	}

	public void displayInDescOrder() {
		List<Book> books2 = new ArrayList<>();
		for (Map.Entry<Integer, Book> map : books.entrySet()) {
			books2.add(map.getValue());
		}
		Collections.sort(books2, (book1, book2) -> book1.getPrice() > book2.getPrice() ? -1 : 1);
		System.out.println(books2);
	}

	public void displayInSoldOrder() {
		List<Book> books2 = new ArrayList<>();
		for (Map.Entry<Integer, Book> map : books.entrySet()) {
			books2.add(map.getValue());
		}
		Collections.sort(books2, (book1, book2) -> book1.getNoOfCopyesSold() > book2.getNoOfCopyesSold() ? -1 : 1);
		System.out.println(books2);
	}

	public static void main(String[] args) {
		MagicOfBooks mb = new MagicOfBooks();
		mb.addBook(1, new Book("a", 200, "horror", 5));
		mb.addBook(2, new Book("b", 100, "thriller", 5));
		mb.addBook(3, new Book("c", 50, "horror", 5));
		mb.addBook(4, new Book("d", 150, "comedy", 5));
		mb.addBook(5, new Book("e", 30, "comedy", 5));
		mb.addBook(6, new Book("f", 110, "romance", 5));
		
		
		mb.display();
	    mb.displayByGenre("comedy");
		mb.displayInDescOrder();
		mb.displayInOrder();
		mb.displayInSoldOrder();
		mb.deleteBook(2);
		mb.display();
		mb.display();
		mb.updateBook(2, new Book("g",700,"forensic",5));
		mb.display();
		mb.countOfBooks();
		
		mb.deleteBook(3);
		mb.display();
		
		

	}

}